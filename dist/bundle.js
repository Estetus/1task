/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./app/modal/modalService.ts":
/*!***********************************!*\
  !*** ./app/modal/modalService.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, exports) => {

eval("\nObject.defineProperty(exports, \"__esModule\", ({ value: true }));\nwindow['openModal'] = openModal;\nwindow['removeModal'] = removeModal;\nwindow['openModalSecond'] = openModalSecond;\nfunction openModal(id) {\n    const template = \"<div>MyModal</div>\";\n    const modal = new ModalService(id);\n    modal.open(template);\n}\nfunction removeModal() {\n    ModalService.removeById();\n}\nfunction openModalSecond(id = null) {\n    const template = \"<div>MyModal 2</div>\";\n    const modal = new ModalService(id);\n    modal.open(template);\n}\nclass ModalService {\n    id;\n    static modals = []; // массив всех экземпляров класса modalService;\n    constructor(id = null) {\n        const findModal = ModalService.modals.find(x => x.id === id);\n        if (!findModal) {\n            ModalService.removeById(id);\n        }\n        ModalService.modals.push(this);\n        this.id = id || (Math.random() + ModalService.modals.length);\n    }\n    open(template) {\n        const divWrap = document.createElement(\"div\");\n        divWrap.innerHTML = template;\n        divWrap.id = this.id;\n        divWrap.setAttribute('modal-id', this.id);\n        divWrap.classList.add(\"modal-element\");\n        document.body.appendChild(divWrap);\n    }\n    remove() {\n        const modalEl = document.getElementById(this.id);\n        modalEl.parentNode.removeChild(modalEl);\n    }\n    static removeById(id = null) {\n        let modalId = id;\n        const findEl = ModalService.modals.find(x => x.id === modalId);\n        if (findEl) {\n            findEl.remove();\n            ModalService.modals = ModalService.modals.filter((el) => el.id !== modalId);\n        }\n        else {\n            if (Array.isArray(ModalService.modals)) {\n                const lastEl = ModalService.modals.pop();\n                if (lastEl) {\n                    lastEl.remove();\n                }\n            }\n        }\n    }\n}\n\n\n//# sourceURL=webpack://typescrypt/./app/modal/modalService.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./app/modal/modalService.ts"](0, __webpack_exports__);
/******/ 	
/******/ })()
;