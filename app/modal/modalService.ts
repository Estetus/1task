import {Modal} from "../classes/modal";
window['openModal'] = openModal;
window['removeModal'] = removeModal;
window['openModalSecond'] = openModalSecond;


function  openModal (id: string):void {
    const template = "<div>MyModal</div>";
    const modal = new ModalService(id);
    modal.open(template);

}

function  removeModal():void {
    ModalService.removeById();
}

function openModalSecond(id = null):void {
    const template = "<div>MyModal 2</div>";
    const modal = new ModalService(id);
    modal.open(template);

}

class ModalService {
    private readonly id: string;
    public static modals:any[] = [] // массив всех экземпляров класса modalService;

    constructor(id = null) {
        const findModal = ModalService.modals.find(x => x.id === id);
        if (!findModal) {
            ModalService.removeById(id);

        }
        ModalService.modals.push(this);
        this.id = id || (Math.random() + ModalService.modals.length)
    }


    public open (template: string): void {
        const divWrap = document.createElement("div");
        divWrap.innerHTML = template;
        divWrap.id = this.id;
        divWrap.setAttribute('modal-id', this.id)
        divWrap.classList.add("modal-element");
        document.body.appendChild(divWrap);
    }
    public remove():void {
        const modalEl = document.getElementById(this.id);
        modalEl.parentNode.removeChild(modalEl);
    }
    public  static removeById(id: string = null): void {
        let modalId = id;

        const findEl = ModalService.modals.find(x => x.id === modalId);
        if(findEl) {
            findEl.remove();
            ModalService.modals = ModalService.modals.filter((el) => el.id !==modalId);
        } else {
            if(Array.isArray(ModalService.modals)) {
                const lastEl = ModalService.modals.pop();
                if(lastEl) {
                    lastEl.remove();


        }
    }

}}}