"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function openModal(id) {
    const template = "<div>MyModal</div>";
    const modal = new ModalService(id);
    modal.open(template);
}
function removeModal() {
    ModalService.removeById();
}
function openModalSecond(id = null) {
    const template = "<div>MyModal 2</div>";
    const modal = new ModalService(id);
    modal.open(template);
}
class ModalService {
    id;
    static modals = []; // массив всех экземпляров класса modalService;
    constructor(id = null) {
        const findModal = ModalService.modals.find(x => x.id === id);
        if (!findModal) {
            ModalService.removeById(id);
        }
        ModalService.modals.push(this);
        this.id = id || (Math.random() + ModalService.modals.length);
    }
    open(template) {
        const divWrap = document.createElement("div");
        divWrap.innerHTML = template;
        divWrap.id = this.id;
        divWrap.setAttribute('modal-id', this.id);
        divWrap.classList.add("modal-element");
        document.body.appendChild(divWrap);
    }
    remove() {
        const modalEl = document.getElementById(this.id);
        modalEl.parentNode.removeChild(modalEl);
    }
    static removeById(id = null) {
        let modalId = id;
        const findEl = ModalService.modals.find(x => x.id === modalId);
        if (findEl) {
            findEl.remove();
            ModalService.modals = ModalService.modals.filter((el) => el.id !== modalId);
        }
        else {
            if (Array.isArray(ModalService.modals)) {
                const lastEl = ModalService.modals.pop();
                if (lastEl) {
                    lastEl.remove();
                }
            }
        }
    }
}
